'use strict';
module.exports = (sequelize, DataTypes) => {
  var images = sequelize.define('images', {
    path: DataTypes.STRING,
    owner_unique: DataTypes.STRING,
    owner_name: DataTypes.STRING,
    ip: DataTypes.STRING,
    filesize: DataTypes.INTEGER,
    filename: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return images;
};